﻿# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

- 学号：202010414417    姓名：王浩帆  班级：软件工程4班

### 实验目的

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

### 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

### 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

### 步骤

#### 1.创建表空间

```
-- 创建sales_data表空间
CREATE TABLESPACE sales_data
  DATAFILE 'sales_data.dbf'
  SIZE 100M
  AUTOEXTEND ON
  NEXT 100M
  MAXSIZE UNLIMITED
  EXTENT MANAGEMENT LOCAL;
![](sales_data表空间.png)

-- 创建sales_index表空间
CREATE TABLESPACE sales_index
  DATAFILE 'sales_index.dbf'
  SIZE 100M
  AUTOEXTEND ON
  NEXT 100M
  MAXSIZE UNLIMITED
  EXTENT MANAGEMENT LOCAL;
将数据表分配到sales_data_ts表空间，用于存储实际的销售数据。
将索引表分配到sales_index_ts表空间，用于加速查询操作。
```
![](sales_index表空间.png)


#### 2.创建表
```
在sales_data表空间中创建以下表：
-- 创建product表
CREATE TABLE products (
  product_id   NUMBER,
  name         VARCHAR2(100),
  price        NUMBER,
  stock        NUMBER
)
```

TABLESPACE sales_data;
![](product表创建.png)

```
-- 创建customers表
CREATE TABLE customers (
  customer_id  NUMBER,
  name         VARCHAR2(100),
  contact      VARCHAR2(100),
)
TABLESPACE sales_data;
```

![](customer表创建.png)

在sales_index表空间中创建以下表：  

```
-- 创建orders表
CREATE TABLE sales_orders (
  order_id     NUMBER,
  customer_id  NUMBER,
  order_date   DATE,
)
TABLESPACE sales_index;
```

![](orders表创建.png)  

```
-- 创建order_items表
CREATE TABLE order_items (
  item_id      NUMBER,
  order_id     NUMBER,
  product_id   NUMBER,
  quantity     NUMBER,
  -- 其他字段
)
TABLESPACE sales_index;
```

![](order_items表创建.png)  

```
表设计：
商品表（product）：
列：product_id（主键）、name、price、stock。
客户表（customers）：
列：customer_id（主键）、name、contact。
订单表（orders）：
列：order_id（主键）、customer_id（外键）、order_date。
订单详情表（order_details）：
列：order_id（外键）、product_id（外键）、quantity、item_id。
```




#### 3.插入数据
商品表

```
--为商品表插入10w条数据
BEGIN
    FOR i IN 1..100000 LOOP
        INSERT INTO Product (product_id, name, price, stock)
        VALUES (i, 'Product ' || i, ROUND(DBMS_RANDOM.VALUE(10, 100), 2), ROUND(DBMS_RANDOM.VALUE(0, 1000)));
        COMMIT;
    END LOOP;
END;
/
--商品表插入数据
```

![](product数据插入.png)

顾客表

```
-- 为客户表插入5w数据
DECLARE
  i NUMBER;
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO TB_CUSTOMER (id, customer_no, name,phone,address)
    VALUES (i, 'cust('100',i)','张三 ' || i, FLOOR(dbms_random.value(1000000000,20000000000),concat('xxx地址00',i));
  END LOOP;
  COMMIT;
END
```

![](customer数据插入.png)

订单表

```
--为订单表插入10w数据
INSERT INTO TB_ORDER1 (order_id, product_id, quantity, price)
SELECT level, mod(level, 1000)+1, mod(level, 10)+1, mod(level, 100)+1
FROM dual
CONNECT BY level <= 20000;
```

![](orders数据插入.png)

#### 4.设计权限及用户分配

```
 --创建管理员用户
CREATE TB_USER admin IDENTIFIED BY pwd;
GRANT CONNECT, RESOURCE, DBA TO admin;
 --创建普通用户
CREATE TB_USER yxl IDENTIFIED BY pwd;
GRANT CONNECT, RESOURCE TO yxl;
GRANT SELECT, INSERT, UPDATE, DELETE ON products TO yxl;
GRANT SELECT, INSERT, UPDATE, DELETE ON customers TO yxl;
GRANT SELECT, INSERT, UPDATE, DELETE ON orders TO yxl;
GRANT SELECT, INSERT, UPDATE, DELETE ON order_details TO yxl;
```

![](权限分配.png)

#### 5.PL/SQL设计

1.创建程序包

```
-- 创建程序包
CREATE OR REPLACE PACKAGE SalesPackage AS
    -- 存储过程：生成订单
    PROCEDURE create_order(p_customer_id NUMBER, p_product_id NUMBER);

    -- 函数：计算订单总价
    FUNCTION calculate_order_total(p_order_id NUMBER) RETURN NUMBER;

    -- 函数：查询用户购买记录
    FUNCTION get_customer_purchase_history(p_customer_id NUMBER) RETURN SYS_REFCURSOR;
END SalesPackage;
/
```

![](创建程序包.png)

2.函数设计

```
CREATE OR REPLACE PACKAGE BODY SalesPackage AS
    -- 存储过程：生成订单
    PROCEDURE create_order(p_customer_id NUMBER, p_product_id NUMBER) IS
    BEGIN
        INSERT INTO orders (customer_id, product_id, order_date)
        VALUES (p_customer_id, p_product_id, SYSDATE);
        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            RAISE;
    END create_order;
    -- 函数：计算订单总价
    FUNCTION calculate_order_total(p_order_id NUMBER) RETURN NUMBER IS
        v_total NUMBER;
    BEGIN
        SELECT SUM(price * quantity) INTO v_total
        FROM order_items
        WHERE order_id = p_order_id;   
        RETURN v_total;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN 0; -- 如果订单不存在或没有商品项，返回总价为0
    END calculate_order_total;
    -- 函数：查询用户购买记录
    FUNCTION get_customer_purchase_history(p_customer_id NUMBER) RETURN SYS_REFCURSOR IS
        v_cursor SYS_REFCURSOR;
    BEGIN
        OPEN v_cursor FOR
            SELECT o.order_id, p.product_name, oi.price, oi.quantity
            FROM orders o
            JOIN order_items oi ON o.order_id = oi.order_id
            JOIN products p ON oi.product_id = p.product_id
            WHERE o.customer_id = p_customer_id;
        RETURN v_cursor;
    END get_customer_purchase_history;
END SalesPackage;
```

![](函数设计.png)



#### 6.备份方案

> 以下是一个包含核心代码的基本数据库备份方案
>
> 1.数据库备份脚本（backup_script.sh）：
>
> ```bash
> #!/bin/bash
> 
> # 设置备份目录和文件名
> BACKUP_DIR="/path/to/backup"
> BACKUP_FILE="database_backup_$(date +%Y%m%d).dmp"
> 
> # 使用Oracle Data Pump进行全量备份
> expdp system/password@db_name DIRECTORY=backup_dir DUMPFILE=$BACKUP_DIR/$BACKUP_FILE FULL=Y
> 
> # 检查备份是否成功
> if [ $? -eq 0 ]; then
>     echo "数据库备份成功：$BACKUP_DIR/$BACKUP_FILE"
> else
>     echo "数据库备份失败"
> fi
> 
> # 删除过期的备份文件
> find $BACKUP_DIR -type f -name 'database_backup_*' -mtime +30 -exec rm {} \;
> ```
>
> 2. 定期备份策略：
>    - 使用cron定时任务执行备份脚本，例如每周日凌晨进行全量备份。
>
> 3. 存储设备和位置：
>    - 将备份文件保存在指定的备份目录（`/path/to/backup`）中。
>
> 4. 备份周期和保留策略：
>    - 每周进行一次全量备份，保留最近30天的备份文件。
>    - 在备份脚本中使用`find`命令，删除30天前的备份文件。
>
> 5. 备份文件的安全性：
>    - 可以在脚本中添加加密备份文件的步骤，使用加密工具（如GPG）对备份文件进行加密。
>
> 6. 监控和日志记录：
>    - 在备份脚本中添加日志记录，将备份操作的详细信息写入日志文件。
>
> 7. 灾难恢复策略：
>    - 根据需要，编写数据库恢复脚本，以从备份文件中还原数据库。
>
> 数据库备份在商品销售系统中至关重要，它提供了数据保护、业务连续性、灾难恢复、合规性和测试等多重好处，确保系统的可靠性、安全性和可用性，定期的备份策略是维护和管理数据库的重要组成部分。

### 总结

实验分析与总结：

在设计基于Oracle数据库的商品销售系统的数据库方案时，我进行了以下步骤：

1. 表空间设计：创建了两个表空间，分别用于存储销售数据和索引数据，以提高查询性能并有效管理数据库存储空间。

2. 表设计：设计了四张表，包括商品表、客户表、订单表和订单详情表。这些表之间通过主键-外键关系建立了关联，形成了数据库的基本数据结构。

3. 用户和权限分配：创建了两个用户，分别是admin和customer。admin用户拥有完全的数据库权限，用于管理系统的数据库对象；customer用户用于执行业务操作，只被授予了对特定表的增删改查权限，以保证数据的安全性和隔离性。

4. 程序包设计：创建了一个名为sales_pkg的程序包，封装了一些存储过程和函数，实现了复杂的业务逻辑。这些存储过程和函数可以方便地被调用，提供了对订单、商品和客户等数据的处理和查询功能。

5. 数据库备份方案：提出了一套数据库备份方案，包括定期全量备份、使用数据泵工具进行导入和导出、保存备份文件到独立存储设备或云存储，并测试备份的可恢复性。这样可以保证数据库数据的安全性和可靠性，以及在灾难发生时能够快速恢复数据库。

通过实验分析，得出以下结论：

1. 合理的表空间设计和分配可以提高数据库的性能和管理效率。通过将数据和索引分开存储，可以避免数据碎片和提升查询速度。

2. 使用用户和权限的分配方案可以保证数据的安全性和隔离性。将用户的权限控制在必要的范围内，避免非授权用户对数据库进行操作。

3. 程序包的使用可以简化复杂业务逻辑的实现。将相关的存储过程和函数封装在程序包中，可以提高代码的可维护性和重用性。

4. 数据库备份是数据库管理的重要环节。合理的备份策略和方案可以保证数据的完整性和可恢复性，防止数据丢失和灾难发生时的数据库故障。

总之，经过这次比较完整的项目，我学习到了Oracle数据库的常用知识以及数据库备份的相关知识，这在MySQL学习中没有涉及到，极大扩展了我的知识面，收获很大，也希望未来可以用这些知识去解决实际问题。
